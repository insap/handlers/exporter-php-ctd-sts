<?php

namespace App;

use App\Illuminate\StorageParams;

class Exporter
{
    protected StorageParams $storageParams;

    public function __construct(StorageParams $storageParams)
    {
        $this->storageParams = $storageParams;
    }

    public function export() : array
    {
        $data = $this->storageParams->data;

        $stationsStart = (int)$this->storageParams->params->get('start', 0);
        $stationsEnd = (int)$this->storageParams->params->get('end', 0);

        if ($stationsStart > 0 && $stationsEnd > 0) {
            $data = $data->filter(fn(array $item) => (int)$item['station'] >= $stationsStart && (int)$item['station'] <= $stationsEnd);
        }

        return ['data' => $data->all()];
    }
}